/*
Seus colegas de classe pediram para você copiar alguns documentos para eles. Você sabe que existem 'n' colegas de
classe e a papelada tem 'm' páginas. Sua tarefa é calcular quantas páginas em branco você precisa.
*/
Exemplo:
function paperwork(n, m) {
    //  não existem colegas de classe negativos
    if ( n < 0 || m < 0 ) {
    return 0
    } else {
// n sao os colegas, m sao as paginas ou seja paperwork(,5 5 0 = 25)
        return n * m
    }

}
// outra maneira por ternario
function paperwork(n, m) {
    //  retorna n > 0 && m > 0 ? n * m : 0
    return n > 0 && m > 0 ? n * m : 0
  }
