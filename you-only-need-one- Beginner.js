// Você receberá uma matriz ae um valor x. Tudo o que você precisa fazer é verificar se a matriz fornecida contém o valor.

// A matriz pode conter números ou seqüências de caracteres. X pode ser também.

// Retorne true se a matriz contiver o valor, false se não.

function check(array, valor) {
    // your code here
    // se  o valor estiver contido na matrix retorna verdadeiro,s e não falso
    // includes compara se o valor está contido mesmo na matriz
    // 
    if(array.includes(valor)) {
    return true 
  } else {
  return false
  }
  }
//   metódo com arrow function, não precisa dar return
  const check = (a,x) => a.includes(x)
 
