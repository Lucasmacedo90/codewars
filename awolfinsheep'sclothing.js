/*
    Um lobo em pele de cordeiro
        Lobos foram reintroduzidos na Grã-Bretanha. Você é um criador de ovelhas e agora é atormentado por lobos que
        fingem ser ovelhas. Felizmente, você é bom em identificá-los.

        Avise as ovelhas na frente do lobo que ele está prestes a ser comido. Lembre-se de que você está em pé na frente
        da fila, que fica no final da matriz:

        [sheep, sheep, sheep, sheep, sheep, wolf, sheep, sheep] (YOU ARE HERE AT THE FRONT OF THE QUEUE)
        7 6 5 4 3 2 1
        Se o lobo é o animal mais próximo de você, volte "Pls go away and stop eating my sheep". Caso contrário, retorne
        "Oi! Sheep number N! You are about to be eaten by a wolf!"onde Nestá a posição da ovelha na fila.

        Nota: sempre haverá exatamente um lobo na matriz.

        Exemplos
        warnTheSheep(["sheep", "sheep", "sheep", "wolf", "sheep"]) === "Oi! Sheep number 1! You are about to be eaten by
        a wolf!"

        warnTheSheep(["sheep", "sheep", "wolf"]) === "Pls go away and stop eating my sheep"
        tests
         Test.assertEquals(warnTheSheep(["sheep", "sheep", "sheep", "sheep", "sheep", "wolf", "sheep", "sheep"]), "Oi! Sheep number 2! You are about to be eaten by a wolf!");
    Test.assertEquals(warnTheSheep(["sheep", "wolf", "sheep", "sheep", "sheep", "sheep", "sheep"]), "Oi! Sheep number 5! You are about to be eaten by a wolf!");
    Test.assertEquals(warnTheSheep(["wolf", "sheep", "sheep", "sheep", "sheep", "sheep", "sheep"]), "Oi! Sheep number 6! You are about to be eaten by a wolf!");
    Test.assertEquals(warnTheSheep(["sheep", "wolf", "sheep"]), "Oi! Sheep number 1! You are about to be eaten by a wolf!");
    Test.assertEquals(warnTheSheep(["sheep", "sheep", "wolf"]), "Pls go away and stop eating my sheep");
*/
function warnTheSheep(queue) {
    // procura o lobo
    const index=queue.indexOf('wolf')
    // se ele estiver a distancia de um indice dentro do array ai retorna a frase
    return index===queue.length-1?"Pls go away and stop eating my sheep"
    // 
    :`Oi! Sheep number ${queue.length-1-index}! You are about to be eaten by a wolf!`
  }