// Você está criando um jogo "Escape the room". 
// O primeiro passo é criar uma tabela de hash chamada roomsque contém todas as salas do jogo.
//  Deve haver, pelo menos, 3 quartos dentro dela,
//  sendo cada uma tabela hash com pelo menos
//   três propriedades (por exemplo name, description, completed).
// criar 3 quartos
// criar objetos 3 objetos dentro de um objeto
let rooms = {
    Firstroom: {
        name: "room1",
        description: "description1",
        competed: true
    },
    Secondroom: {
        name: "room2",
        description: "description2",
        competed: false
    },
    Thirdroom: {
        name: "room3",
        description: "description3",
        competed: true
    }
}
// loop for criar 3 quartos
let rooms = {}
// percorre a criação de quartos
for (let i = 0; i < 3; i++) {
    // transforma os quartos em array de object
    // cada loop cria um quarto
    rooms[`room${i}`] = { 'a': 1, 'b': 2, 'c': 3 }
}
// método com array
const rooms = {
    room1: [1, 2, 3],
    room2: [4, 5, 6],
    room3: [7, 8, 9]
}