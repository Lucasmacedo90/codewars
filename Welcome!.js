/*
O BA da sua empresa disse ao marketing que seu site tem um grande público na Escandinávia e nos países vizinhos.
        O marketing acha que seria ótimo receber os visitantes do site em seu próprio idioma. Felizmente, você já usa
        uma API que detecta a localização do usuário, portanto é uma vitória fácil.

        A tarefa
        Pense em uma maneira de armazenar os idiomas como um banco de dados (por exemplo, um objeto). Os idiomas estão
        listados abaixo para que você possa copiar e colar!
        Escreva uma função de 'bem-vindo' que use um parâmetro 'idioma' (sempre uma string) e retorne uma saudação - se
        você a tiver no seu banco de dados. Ele deve usar como padrão o inglês se o idioma não estiver no banco de dados
        ou no caso de uma entrada inválida.
        O banco de dados
        english: 'Welcome',
        czech: 'Vitejte',
        danish: 'Velkomst',
        dutch: 'Welkom',
        estonian: 'Tere tulemast',
        finnish: 'Tervetuloa',
        flemish: 'Welgekomen',
        french: 'Bienvenue',
        german: 'Willkommen',
        irish: 'Failte',
        italian: 'Benvenuto',
        latvian: 'Gaidits',
        lithuanian: 'Laukiamas',
        polish: 'Witamy',
        spanish: 'Bienvenido',
        swedish: 'Valkommen',
        welsh: 'Croeso'

        Possíveis entradas inválidas incluem:
        IP_ADDRESS_INVALID - not a valid ipv4 or ipv6 ip address
        IP_ADDRESS_NOT_FOUND - ip address not in the database
        IP_ADDRESS_REQUIRED - no ip address was supp
*/

function greet(language) {
	let bancodedados = {
// criar banco de dados como object
english: 'Welcome',
czech: 'Vitejte',
danish: 'Velkomst',
dutch: 'Welkom',
estonian: 'Tere tulemast',
finnish: 'Tervetuloa',
flemish: 'Welgekomen',
french: 'Bienvenue',
german: 'Willkommen',
irish: 'Failte',
italian: 'Benvenuto',
latvian: 'Gaidits',
lithuanian: 'Laukiamas',
polish: 'Witamy',
spanish: 'Bienvenido',
swedish: 'Valkommen',
welsh: 'Croeso'
}
// percorrer todo o banco de dados em array procurando os elementos
for ( let chave in bancodedados) {
// condição se a chave for igual ao idioma (propriedade do object)
    if(chave == language) {
    // retornar a propriedade do object
        return bancodedados[chave]
    }
}
    // ao retornar a propriedade, retorna o valor da propriedade do object
    return bancodedados['english']
}
// outro modo de resolver com switch
function greetings(language) {
    let database = [
  'Vitejte',
  'Velkomst',
  'Welkom',
  'Tere tulemast',
  'Tervetuloa',
  'Welgekomen',
  'Bienvenue',
  'Willkommen',
  'Failte',
  'Benvenuto',
  'Gaidits',
  'Laukiamas',
  'Witamy',
  'Bienvenido',
  'Valkommen',
  'Croeso'];
   switch(language) {
    // caso na posição 0 da matriz retorna o elemento no indice [0], no caso 'Vitejte'
       case 0: return database[0];
       case 1: return database[1];
       case 2: return database[2];
       case 3: return database[3];
       case 4: return database[4];
       case 5: return database[5];
       case 6: return database[6];
       case 7: return database[7];
       case 8: return database[8];
       case 9: return database[9];
       case 10: return database[10];
       case 11: return database[11];
       case 12: return database[12];
       case 13: return database[13];
       case 14: return database[14];
       case 15: return database[15];
       case 16: return database[16];
       default: 'Welcome'
   }
}