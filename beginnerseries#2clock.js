/* Beginner Series #2 Clock
O relógio mostra 'h' horas, 'm' minutos e 's' segundos após a meia-noite.
Sua tarefa é criar a função 'Passado', que retorna o tempo convertido em milissegundos.
Exemplo:
past(0, 1, 1) == 61000
Restrições de entrada: 0 <= h <= 23, 0 <= m <= 59,0 <= s <= 59
*/



function past(h, m, s){
    //#Happy Coding! ^_^
  //   apenas converter tudo para milisegundo
    return (3600 * 1000 * h) + (60 * 1000 * m) + (1000 * s)
  };
  let clock = past(2, 3, 4);
  console.log('clock', clock)

// outro modo
function past(h, m, s){
    // date() cria uma data e sethours() seta os horários
    
    const setTime = new Date().setHours(h, m, s);
    const midnight = new Date().setHours(0, 0, 0);
    // arredonda para o inteiro mais próximo, entretanto é melhor usar o Math.ceil
    // o set time - midnight
    return Math.round(setTime - midnight)
  }
  let clock2 = past(2, 3, 4)
//   outra maneira
const setTime = new Date().setHours(2, 3, 4);
const midnight = new Date().setHours(0, 0, 0);
let calcclock2 = Math.round(setTime - midnight);
 console.log(clock2)
  console.log(calcclock2)