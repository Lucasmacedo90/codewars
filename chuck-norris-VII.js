// É um fato bem conhecido que qualquer coisa que Chuck Norris queira, ele recebe. Como resultado, Chuck raramente usa a palavra falso.

// É um fato menos conhecido que, se algo é verdade, e Chuck não quer que seja, Chuck pode assustar a verdade com seus enormes bíceps, e ela automaticamente se torna falsa.

// Sua tarefa é ser mais como Chuck (ha! Boa sorte!). Você deve retornar false sem nunca usar a palavra false ...

// Vá mostrar alguma verdade quem é que manda!

function ifChuckSaysSo(){
    // retorna não verdadeiro
    return !true
    }
    // arrow function
    ifChuckSaysSo = () => !true;
    // outra forma 5 > 10 é falso,
    // logo retorna falso
    function ifChuckSaysSo() {
        return 5 > 10; //lol
      }