// Escreva uma função para converter um nome em iniciais. Este kata leva estritamente duas palavras com um espaço entre elas.

// A saída deve ser duas letras maiúsculas com um ponto separando-as.

// Deve ficar assim:

// Sam Harris => S.H

// Patrick Feeney => P.F

// 1 método com loop for
function abbrevName(name){

    // code away
// explodir a string em letras
let parts = name.split(' ').replace(' ',/\./g)
// armazenar
let initials = ' '
// percorrer todo as letras
for (let i = 0; i < parts.length; i++) {
// condição
if (parts[i].length > 0 && parts[i] !== '') {
    initials += parts[i][0]
}
}
return initials
}
// outro método com chart
// chartAt retorna o caracter do indice
let initials2 = item.FirstName.charAt(0).toUpperCase() + 
                item.LastName.charAt(0).toUpperCase();
console.log(initials2)
// com switch
function getInitials(name) {
let array = name.split(" ");

        switch ( array.length ) {

            case 1:
                // array[0] encontra o elemento da  primeira posição do array
                // chartAt retira o elemento da primeira posição
                // toUpperCase() deixa maiusculo
                return array[0].charAt(0).toUpperCase();
                break;
            default:
                return array[0].charAt(0).toUpperCase() + array[ array.length -1 ].charAt(0).toUpperCase();
        }

}

function abbrevName2(name) {
    // uppercase deixa maiusculo, split explode os nomes em caracteres
    [first, last] = name.toUpperCase().split(' ');
    // por serem 2 nomes posso chamar o primeiro de first e o segundod e last
    // retorno o indice 0 do primeiro nome e o indice 0 do segundo
    return first[0] + '.' + last[0];
    // indice 0 é  a primeira posiçãod e um array
  }