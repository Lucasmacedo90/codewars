// tentar entender
//define the string prototype here
String.prototype.isUpperCase = function(){
    if(this.toString() == this.toUpperCase()){
      return true;
    } else{
      return false;
    }
  }
//   ou por Regex
String.prototype.isUpperCase = function() {
    return this.search(/[a-z]/g) == -1;
  }
//   ou ainda por ternário
String.prototype.isUpperCase = function() {
  
    return this == this.toUpperCase() ? true : false;
  }